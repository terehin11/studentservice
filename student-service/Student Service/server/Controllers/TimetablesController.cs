// using System;
// using System.Collections.Generic;
// using Microsoft.AspNetCore.Mvc;
// using WebApi.Services;
// using WebApi.Dtos;
// using AutoMapper;
// using System.IdentityModel.Tokens.Jwt;
// using WebApi.Helpers;
// using Microsoft.Extensions.Options;
// using System.Text;
// using Microsoft.IdentityModel.Tokens;
// using System.Security.Claims;
// using WebApi.Entities;
// using Microsoft.AspNetCore.Authorization;

// namespace WebApi.Controllers
// {

//     [Route("[controller]")]
//     public class TimetablesController: Controller 
//     {
//         private ITimetableService _timetableService;
//         private IMapper _mapper;
//         public TimetablesController (
//             ITimetableService timetableService,
//             IMapper mapper
//         )
//         {
//             _timetableService = timetableService;
//             _mapper = mapper;
//         }

//         [HttpGet]
//         public IActionResult GetTimetables()
//         {
//             var timetables = _timetableService.GetTimetables();
//             var timetablesDto = _mapper.Map<IList<TimetableDto>>(timetables);
//             return Ok(timetablesDto);
//         }
//     }
// }
