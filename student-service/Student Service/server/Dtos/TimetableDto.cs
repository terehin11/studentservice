namespace WebApi.Dtos
{
    public class TimetableDto
    {
        public string NameSubject {get; set;}
        public string Time {get;set;}
        public string NameTeacher {get;set;}
        public string ClassRoom {get;set;}
        public string Group {get;set;}
    }
}