import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import {
    Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem,
    NavLink,
    NavbarText
  } from 'reactstrap';
import { userActions } from '../_actions';

class Menu extends React.Component {
    render() {
        let menus = [
            "Subject",
            "Timetable",
            "Literature",
            "User"
        ]
        return <div>
            {menus.map((value,index)=>{
                return <div key={index}><LinkMenu label={value}/></div>
            })}
        </div>
    }

}

class LinkMenu extends React.Component {
    render() {
        const url="/api/v1/" + this.props.label.toLowCase().trim().replace(" ","-")
        return <div> 
            <a href={url}>{this.props.label}</a>
        </div>
    }
}

class HomePage extends React.Component {
    componentDidMount() {
        this.props.dispatch(userActions.getAll());
    }

    handleDeleteUser(id) {
        return (e) => this.props.dispatch(userActions.delete(id));
    }

    render() {
        const { user, users } = this.props;
        return (
            <div className="col-md-6 col-md-offset-3">
                <Navbar color="light" light expand="md">
        <NavbarBrand href="/subjects">Subjects</NavbarBrand>
        <NavbarToggler onClick={toggle} />
        <Collapse isOpen={isOpen} navbar>
          <Nav className="mr-auto" navbar>
            <NavItem>
              <NavLink href="/timetable">Timetable</NavLink>
            </NavItem>
            <NavItem>
              <NavLink href="/literature">Literature</NavLink>
            </NavItem>
            <NavItem>
                <NavLink href="/login">Logout</NavLink>
            </NavItem>
          </Nav>
          <NavbarText>Student Service</NavbarText>
        </Collapse>
      </Navbar>
                <h1>Hi {user.firstName}!</h1>
                <h3>All registered users:</h3>
                {users.loading && <em>Loading users...</em>}
                {users.error && <span className="text-danger">ERROR: {users.error}</span>}
                {users.items &&
                    <ul>
                        {users.items.map((user, index) =>
                            <li key={user.id}>
                                {user.firstName + ' ' + user.lastName}
                                {
                                    user.deleting ? <em> - Deleting...</em>
                                    : user.deleteError ? <span className="text-danger"> - ERROR: {user.deleteError}</span>
                                    : <span> - <a onClick={this.handleDeleteUser(user.id)}>Delete</a></span>
                                }
                            </li>
                        )}
                    </ul>
                }
                <p>
                    <Link to="/login">Logout</Link>
                </p>
            </div>
        );
    }
}

function mapStateToProps(state) {
    const { users, authentication } = state;
    const { user } = authentication;
    return {
        user,
        users
    };
}

const connectedHomePage = connect(mapStateToProps)(HomePage);
export { connectedHomePage as HomePage };